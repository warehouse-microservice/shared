module github.com/SysIntruder/warehouse

go 1.21.6

require (
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.3.0 // indirect
	google.golang.org/protobuf v1.32.0 // indirect
)
