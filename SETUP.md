# Setting Up Project (Linux, Arch btw)

## Setting $PATH
make sure your path contains `/{user}/go/bin` and `/{user}/bin`

## Install Make

```
sudo pacman -S make
```

## Install Docker

```
sudo pacman -S docker docker-buildx docker-compose
systemctl start docker
```

## Install Protobuf and gRPC
```
sudo pacman -S protobuf
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
```

## Install Templ

```
go install github.com/a-h/templ/cmd/templ@latest
```

## Install Tailwind CLI
```
cd $HOME/bin
curl -sLO https://github.com/tailwindlabs/tailwindcss/releases/latest/download/tailwindcss-linux-x64
chmod +x tailwindcss-linux-x64
mv tailwindcss-linux-x64 tailwindcss
```
