gen: gen-warehouse gen-vendor gen-customer gen-api-gateway

gen-warehouse:
	@protoc --go_out ./service-warehouse/ ./shared-proto/warehouse/warehouse.proto
	@protoc --go-grpc_out ./service-warehouse/ ./shared-proto/warehouse/warehouse.proto

gen-vendor:
	@protoc --go_out ./service-vendor/ ./shared-proto/vendor/vendor.proto
	@protoc --go-grpc_out ./service-vendor/ ./shared-proto/vendor/vendor.proto

gen-customer:
	@protoc --go_out ./service-customer/ ./shared-proto/customer/customer.proto
	@protoc --go-grpc_out ./service-customer/ ./shared-proto/customer/customer.proto

gen-transaction:
	@protoc --go_out ./service-transaction/ ./shared-proto/transaction/transaction.proto
	@protoc --go_out ./service-warehouse/ ./shared-proto/transaction/transaction.proto
	@protoc --go_out ./service-vendor/ ./shared-proto/transaction/transaction.proto
	@protoc --go_out ./service-customer/ ./shared-proto/transaction/transaction.proto
	@protoc --go-grpc_out ./service-transaction/ ./shared-proto/transaction/transaction.proto
	@protoc --go-grpc_out ./service-warehouse/ ./shared-proto/transaction/transaction.proto
	@protoc --go-grpc_out ./service-vendor/ ./shared-proto/transaction/transaction.proto
	@protoc --go-grpc_out ./service-customer/ ./shared-proto/transaction/transaction.proto

gen-api-gateway:
	@protoc --go_out ./api-gateway/ ./shared-proto/warehouse/warehouse.proto
	@protoc --go_out ./api-gateway/ ./shared-proto/vendor/vendor.proto
	@protoc --go_out ./api-gateway/ ./shared-proto/customer/customer.proto
	@protoc --go_out ./api-gateway/ ./shared-proto/transaction/transaction.proto
	@protoc --go-grpc_out ./api-gateway/ ./shared-proto/warehouse/warehouse.proto
	@protoc --go-grpc_out ./api-gateway/ ./shared-proto/vendor/vendor.proto
	@protoc --go-grpc_out ./api-gateway/ ./shared-proto/customer/customer.proto
	@protoc --go-grpc_out ./api-gateway/ ./shared-proto/transaction/transaction.proto
